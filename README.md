# Rustlate

Really simple free google translate library for Rust.

## Links
- [Online Documentation](https://docs.rs/rustlate/)
- [Crates.io](https://crates.io/crates/rustlate)

## Example Usage(s)

```rust
use rustlate;

fn main() {
    let translator_struct = rustlate::Translator{
        to: "tr",
        from: "en"
    };


    match translator_struct.translate("hello.") {
        Ok(translated) => println!("Result: {}", translated),
        Err(_) => println!("Something went wrong...")
    }
}
```

```rust
use rustlate;

fn main() {
    println!("{:?}", rustlate::translate_auto("hello", "tr"));
}
```

## Found a bug? Got an error?

- I'm still learning Rust, so may this library can has some bugs / errors. If you found an one, please open an issue at gitlab repository.

## Contributing

If you want to contribute this project:

- Make sure you add the comments for your codes.
- Please do not something useless.

## Authors

- [Aiocat](https://gitlab.com/aiocat)

## License

This project is distributed under MIT license.

## Project status

Under development.
